package com.thomsonreuters.game;

import com.thomsonreuters.domain.player.Player;

/**
 * The interface CardsGame.
 */
public interface CardsGame {

    /**
     * Gets game name.
     *
     * @return the game name
     */
    String getGameName();

    /**
     * Add player.
     *
     * @param player the player
     */
    void addPlayer(Player player);

    /**
     * Start game.
     */
    void startGame();

    /**
     * Gets winner.
     *
     * @return the winner
     */
    Player getWinner();
}
